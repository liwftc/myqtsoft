# from cgitb import reset
import sys
import os
import math
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from mapview.card import Card
from mapview.timeline import TimeLine
from mapview.mapview import MapView
from qt_material import apply_stylesheet,list_themes
from qdarkstyle import *


class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)


        view = MapView()
        addbutton = QPushButton()
        layout = QVBoxLayout()
        layout.addWidget(addbutton)
        
        
        layout.addWidget(view)
        addbutton.clicked.connect(lambda:view.scene.addItem(Card(0,0)))
        self.widget = QWidget()
        self.widget.setLayout(layout)

        self.setCentralWidget(self.widget)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    # apply_stylesheet(app,'light_amber.xml')
    print(list_themes())
    w = MainWindow()
    w.resize(800,800)
    w.show()
    sys.exit(app.exec())

import sys
import os
import math
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from mapview.card import Card
from mapview.mapconfig import *

class TimeNode(QGraphicsEllipseItem):
    def __init__(self,x:float, y:float, parent=None):
        super().__init__(0, 0, 10, 10, parent)
        print('scale:',mapconfig.get_config('scale'))
        self.setFlags(QGraphicsItem.ItemIsSelectable |
                     QGraphicsItem.ItemIsFocusable |
                     QGraphicsItem.ItemIsMovable)
        self.setPos(self.parentItem().start[0]-5,y)
        print(self.parentItem().start,y)
        self.card = Card(20,0,parent=self)
        # print(self.parentItem().start)
        # print(self.pos())
        # print(self.scenePos())
        
    def mouseMoveEvent(self,event):
        print(self.pos(),self.scenePos(),event.scenePos())
        print(event.setScenePos(QPointF(self.parentItem().start[0],event.scenePos().y())))
        print(event.scenePos())
        print('\n')
        super().mouseMoveEvent(event)
        
    # def itemChange(self, change, value):
    #     print(change)
    #     if change == QGraphicsItem.GraphicsItemChange.ItemPositionChange :
    #         # value is the new position.
    #         pen=QPen()
    #         pen.setColor(Qt.red)
    #         self.setPen(pen)
    #         newPos = value.toPointF()
    #         return QPointF(95,newPos.y())
    #         # rect = scene().sceneRect()
    #     return super().itemChange(change, value)
        
        
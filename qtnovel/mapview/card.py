import sys
from PySide6 import QtWidgets, QtGui, QtCore
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *

class Card(QGraphicsRectItem):
    def __init__(self,x=0,y=0,parent=None):
        super().__init__(0,0,600,100,parent)
        self.setPos(x,y-50)
        self.setFlags(QGraphicsItem.ItemIsSelectable |
                     QGraphicsItem.ItemIsFocusable)

        brush = QBrush(Qt.red)
        self.setBrush(brush)
        label=QLabel('事件')
        textedit =QTextEdit()
        # textedit.resize(100,90)
        layout = QHBoxLayout()  
        for widget in [label,textedit]:
            layout.addWidget(widget)
        widget = QWidget()
        widget.setLayout(layout)
        widget.resize(600,100)
        # widget.setStyleSheet('QLabel {background-color:red}')
        
        w = QGraphicsProxyWidget(self)
        w.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        w.setWidget(widget)
        print(f'card\nscenepos{self.scenePos()}')
        print(self.pos())
    
    def mousePressEvent(self, event):
        print(f'cardscenepos{self.scenePos()}')
        super().mousePressEvent(event)

    # def boundingRect(self):
    #     return QRectF(0, 0, self.width, self.height)

    # def paint(self, painter, option, widget):
    #     pen = QPen()
    #     pen.setWidthF(1)
    #     pen.setColor(self.pingleiColor)
    #     brushColor = self.pingleiColor
    #     if option.state & QStyle.State_Selected:
    #         brushColor = self.pingleiColor.darker(150)
    #     painter.setPen(pen)
    #     painter.setBrush(QBrush(brushColor))
    #     painter.drawRect(0, 0, 100, 200)


if __name__ == "__main__":

    app = QApplication([])
    groupBox = QGroupBox("Contact Details")
    numberLabel = QLabel("Telephone number")
    numberEdit = QLineEdit()
    layout = QFormLayout()
    layout.addRow(numberLabel, numberEdit)
    groupBox.setLayout(layout)
    scene = QGraphicsScene()
    proxy = scene.addWidget(groupBox)
    view = QGraphicsView(scene)
    view.show()
    sys.exit(app.exec())
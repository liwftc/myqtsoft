import sys
import os
import math
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from mapview.timenode import TimeNode

class TimeLine(QGraphicsLineItem):
    def __init__(self,direction, start ,length, parent=None):
        self.start = start
        self.length = length
        if direction == 'horizontal':
            super().__init__(start[0], start[1], start[0]+length, start[1],parent)
        elif direction == 'vertical':
            super().__init__(start[0], start[1], start[0], start[1]+length,parent)
        # self.
        pen=QPen()
        pen.setColor(Qt.blue)
        pen.setWidth(2)
        self.setPen(pen)
        self.nodes=[]
        
    def mousePressEvent(self,event):
        if event.button() == Qt.LeftButton:
            print(f'timeline:{self.scenePos()}')
            timenode=TimeNode(-5,event.pos().y()-5,parent=self)
            self.nodes.append(timenode)
        super().mousePressEvent(event)
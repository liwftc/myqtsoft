import sys
import os
import math
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from mapview.timeline import *
from mapview.mapconfig import *

class MapView(QGraphicsView):
    def __init__(self, parent=None):
        super().__init__(parent)
        mapconfig.set_config('scale',1)
        self.parent = parent
        scene = QGraphicsScene()
        # scene.setSceneRect(0,0,1000,1000)
        scene.setBackgroundBrush(Qt.gray)
        
        scene.addItem(QGraphicsRectItem(0,0,1000,1000))
        printbutton = QPushButton()
        printbutton.clicked.connect(lambda: print(self.viewport().rect(),scene.sceneRect()))
        scene.addWidget(printbutton)
        resetbutton = QPushButton()
        resetbutton.clicked.connect(lambda: print(self.resetTransform()))
        scene.addWidget(resetbutton)
        scene.addItem(TimeLine('vertical',(100,0),5000))
        scene.addItem(TimeLine('horizontal',(100,0),500))
        
        self.setScene(scene)
        self.is_btn_down = False
        self.setDragMode(QGraphicsView.ScrollHandDrag) # 设置可以拖动
        self.mode = 'nottest'
        

    def wheelEvent(self, event):
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        delta = event.angleDelta().y()
        if self.mode=='test':
            print(f'{self.transform()}')
            print(f'm11:{self.transform().m11()}')
            print(f'm12:{self.transform().m12()}')
        scale_change=abs(math.pow(delta/100,delta/abs(delta)))
        self.scale(scale_change,scale_change)
        self.setTransformationAnchor(QGraphicsView.AnchorViewCenter)
        mapconfig.set_config('scale',self.transform().m11())

    def mousePressEvent(self, event):
        item = self.itemAt(event.pos())
        self.item = item
        # if item:
        #     print("You clicked on item", item)
        # else:
        #     if event.button() == Qt.LeftButton:
        #         self.lastpos = event.pos()
        #         self.is_btn_down = True
                # self.centerOn(self.mapToScene(event.pos()))
        super().mousePressEvent(event)


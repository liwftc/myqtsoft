class MapConfig():
    _config={}
    
    def get_config(self,key):
        return self._config[key]
    
    def set_config(self,key,value):
        self._config[key] = value

mapconfig = MapConfig()
    
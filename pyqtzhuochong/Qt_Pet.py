import sys
import os
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtMultimedia import (QAudio, QAudioOutput, QMediaFormat,
                                  QMediaPlayer)
# from qt_material import list_themes
from Ui_mydialog import *
from Ui_settingdialog import *
from qt_material import apply_stylesheet
import qdarkstyle
# list_themes()


class Qt_pet(QWidget):

    def __init__(self):
        super(Qt_pet, self).__init__()
        # self.inpath=os.path.dirname(__file__)
        # apply_stylesheet(self,'light_blue.xml') 
        self.darkstyle=qdarkstyle.load_stylesheet(qt_api='pyside2',palette=qdarkstyle.dark.palette.DarkPalette)
        self.dis_file = "img1"
        self.windowinit()
        self.icon_quit()
        self.pos_first = self.pos()

        self.img_count = len(os.listdir('./image//{}'.format(self.dis_file)))
        self.timer = QTimer()
        self.timer.timeout.connect(self.img_update)
        self.timer.start(10000)

        # self.playmusic()

        self.playtimer = QTimer()
        self.playtimer.timeout.connect(self.playmusic)
        self.is_show = True
        self.is_noti = True
        self.noti_interval = 40
        self.playtimer.start(1000*60*self.noti_interval)

    def windowinit(self):
        self.x = 1800
        self.y = 900
        self.setGeometry(self.x, self.y, 100, 100)
        self.setWindowTitle('My Pet')
        self.img_num = 1
        self.img_path = './image//{file}/{img}.png'.format(
            file=self.dis_file, img=str(self.img_num))
        self.lab = QLabel(self)
        self.lab.setMaximumSize(100, 100)

        self.qpixmap = QPixmap(self.img_path)
        self.lab.setPixmap(self.qpixmap)
        
        self.lab.setScaledContents(True)
        self.setWindowFlags(Qt.FramelessWindowHint |
                            Qt.WindowStaysOnTopHint |
                            Qt.SubWindow)
        self.setAutoFillBackground(False)
        self.setAttribute(Qt.WA_TranslucentBackground, True)
        self.show()

    def img_update(self):
        if self.img_num < self.img_count:
            self.img_num += 1
        else:
            self.img_num = 0
        self.img_path = './image//{file}/{img}.png'.format(
            file=self.dis_file, img=str(self.img_num))
        self.qpixmap = QPixmap(self.img_path)
        self.lab.setPixmap(self.qpixmap)

    def playmusic(self):
        # 播放音乐
        self.player = QMediaPlayer()
        self.audioOutput = QAudioOutput()
        self.audioOutput.setVolume(50)
        self.player.setAudioOutput(self.audioOutput)
        self.player.setSource("朴树 - 平凡之路.mp3")

        print('player')
        self.player.play()

        self.message = QDialog()
        self.mydialog = Ui_Dialog()
        self.mydialog.setupUi(self.message)
        self.message.setWindowFlags(Qt.Tool |  # 确保不关闭主窗口
                                    Qt.WindowMinimizeButtonHint |  # 使能最小化按钮
                                    Qt.WindowCloseButtonHint |  # 使能关闭按钮
                                    Qt.WindowStaysOnTopHint)  # 窗体总在最前端
        self.message.showMaximized()
        self.message.exec()
        self.player.stop()

    def icon_quit(self):
        mini_icon = QSystemTrayIcon(self)

        mini_icon.setIcon(QIcon('./image//img2/1.png'))
        quit_menu = QAction('Exit', self, triggered=self.quit)
        music_menu = QAction('setting', self, triggered=self.opensetbox)
        tpMenu = QMenu(self)
        tpMenu.addAction(quit_menu)
        tpMenu.addAction(music_menu)
        mini_icon.setContextMenu(tpMenu)
        apply_stylesheet(tpMenu,'light_blue.xml') 
        mini_icon.show()

    def opensetbox(self):
        self.setting = QDialog()
        self.setting.accepted.connect(self.set)
        self.setting.ui = Ui_setting()
        self.setting.ui.setupUi(self.setting)
        self.setting.setWindowFlag(Qt.Tool)
        self.setting.ui.is_noti.setChecked(self.is_noti)
        self.setting.ui.is_show.setChecked(self.is_show)
        self.setting.ui.noti_interval.setText(str(self.noti_interval))
        # self.setting.setStyleSheet(self.darkstyle)
        apply_stylesheet(self.setting,'dark_blue.xml')
        self.setting.open()
        

    def set(self):
        self.is_noti = self.setting.ui.is_noti.isChecked()
        self.is_show = self.setting.ui.is_show.isChecked()
        self.noti_interval = int(self.setting.ui.noti_interval.text())

        if self.is_noti == True:
            self.playtimer.start(1000*60*self.noti_interval)
        else:
            print('stop')
            self.playtimer.stop()
        if self.is_show == False:
            self.close()
        else:
            self.show()

    def mousePressEvent(self, QMouseEvent):
        if QMouseEvent.button() == Qt.LeftButton:
            self.pos_first = QMouseEvent.globalPosition().toPoint() - self.pos()
            QMouseEvent.accept()
            self.setCursor(QCursor(Qt.OpenHandCursor))

    def mouseMoveEvent(self, QMouseEvent):
        if Qt.LeftButton:
            self.move(QMouseEvent.globalPosition().toPoint() - self.pos_first)
            self.x, self.y = self.pos().x, self.pos().y
            QMouseEvent.accept()

    def quit(self):
        print('quit')
        self.close()
        sys.exit()


if __name__ == '__main__':
    app = QApplication(sys.argv)
    pet = Qt_pet()
    sys.exit(app.exec())

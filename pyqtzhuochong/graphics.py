from cgitb import reset
import sys
import os
import math
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from card import Card
from timeline import TimeLine
from qt_material import apply_stylesheet


class ZoomView(QGraphicsView):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.parent = parent
        self.is_btn_down = False
        self.setDragMode(QGraphicsView.ScrollHandDrag)
        

    def wheelEvent(self, event):
        self.setTransformationAnchor(QGraphicsView.AnchorUnderMouse)
        delta = event.angleDelta().y()
        print(f'{self.transform()}')
        print(f'm11:{self.transform().m11()}')
        print(f'm12:{self.transform().m12()}')
        scale_change=abs(math.pow(delta/100,delta/abs(delta)))
        self.scale(scale_change,scale_change)
        self.setTransformationAnchor(QGraphicsView.AnchorViewCenter)

    def mousePressEvent(self, event):
        # print(type(event))
        # print(f'fromscen{self.mapToScene(0,0)}')
        # print(f'transform{self.transform()}')
        item = self.itemAt(event.pos())
        self.item = item
        # if item:
        #     print("You clicked on item", item)
        # else:
        #     if event.button() == Qt.LeftButton:
        #         self.lastpos = event.pos()
        #         self.is_btn_down = True
                # self.centerOn(self.mapToScene(event.pos()))
        super().mousePressEvent(event)





class MainWindow(QMainWindow):
    def __init__(self, parent=None):
        QMainWindow.__init__(self, parent)
        scene = QGraphicsScene()
        scene.setBackgroundBrush(Qt.gray)

        view = ZoomView(scene)
        addbutton = QPushButton()
        layout = QVBoxLayout()
        layout.addWidget(addbutton)
        printbutton = QPushButton()
        printbutton.clicked.connect(lambda: print(view.viewport().rect(),scene.sceneRect()))
        resetbutton = QPushButton()
        resetbutton.clicked.connect(lambda: print(view.resetTransform()))

        scene.addWidget(printbutton)
        scene.addWidget(resetbutton)
        scene.addItem(TimeLine(0,0,0,500))
        scene.addItem(QGraphicsRectItem(0,0,100,100))
        
        layout.addWidget(view)
        addbutton.clicked.connect(lambda:scene.addItem(Card(0,0)))
        self.widget = QWidget()
        self.widget.setLayout(layout)

        self.setCentralWidget(self.widget)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    apply_stylesheet(app,'light_blue.xml')
    w = MainWindow()
    w.resize(800,800)
    w.show()
    sys.exit(app.exec())

# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'settingdialog.ui'
##
## Created by: Qt User Interface Compiler version 6.2.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractButton, QApplication, QCheckBox, QDialog,
    QDialogButtonBox, QGroupBox, QHBoxLayout, QLabel,
    QLineEdit, QSizePolicy, QWidget)

class Ui_setting(object):
    def setupUi(self, Dialog):
        if not Dialog.objectName():
            Dialog.setObjectName(u"Dialog")
        Dialog.resize(670, 420)
        self.buttonBox = QDialogButtonBox(Dialog)
        self.buttonBox.setObjectName(u"buttonBox")
        self.buttonBox.setGeometry(QRect(110, 300, 411, 241))
        self.buttonBox.setOrientation(Qt.Vertical)
        self.buttonBox.setStandardButtons(QDialogButtonBox.Cancel|QDialogButtonBox.Ok)
        self.groupBox = QGroupBox(Dialog)
        self.groupBox.setObjectName(u"groupBox")
        self.groupBox.setGeometry(QRect(70, 50, 561, 231))
        self.is_noti = QCheckBox(self.groupBox)
        self.is_noti.setObjectName(u"is_noti")
        self.is_noti.setGeometry(QRect(40, 30, 80, 20))
        self.is_show = QCheckBox(self.groupBox)
        self.is_show.setObjectName(u"is_show")
        self.is_show.setGeometry(QRect(40, 90, 111, 21))
        self.widget = QWidget(self.groupBox)
        self.widget.setObjectName(u"widget")
        self.widget.setGeometry(QRect(180, 30, 249, 23))
        self.horizontalLayout = QHBoxLayout(self.widget)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.label = QLabel(self.widget)
        self.label.setObjectName(u"label")

        self.horizontalLayout.addWidget(self.label)

        self.noti_interval = QLineEdit(self.widget)
        self.noti_interval.setObjectName(u"noti_interval")

        self.horizontalLayout.addWidget(self.noti_interval)


        self.retranslateUi(Dialog)
        self.buttonBox.accepted.connect(Dialog.accept)
        self.buttonBox.rejected.connect(Dialog.reject)

        QMetaObject.connectSlotsByName(Dialog)
    # setupUi

    def retranslateUi(self, Dialog):
        Dialog.setWindowTitle(QCoreApplication.translate("Dialog", u"Dialog", None))
        self.groupBox.setTitle(QCoreApplication.translate("Dialog", u"GroupBox", None))
        self.is_noti.setText(QCoreApplication.translate("Dialog", u"\u662f\u5426\u63d0\u9192", None))
        self.is_show.setText(QCoreApplication.translate("Dialog", u"\u662f\u5426\u663e\u793a\u684c\u5ba0", None))
        self.label.setText(QCoreApplication.translate("Dialog", u"\u63d0\u9192\u95f4\u9694\uff08\u5206\u949f\uff09\uff1a", None))
    # retranslateUi


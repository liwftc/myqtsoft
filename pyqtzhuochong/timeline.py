import sys
from PySide6 import QtWidgets, QtGui, QtCore
from PySide6.QtGui import *
from PySide6.QtCore import *
from PySide6.QtWidgets import *

class TimeLine(QGraphicsLineItem):
    def __init__(self,x1, y1, x2, y2,parent=None):
        QGraphicsLineItem.__init__(self,x1, y1, x2, y2,parent)
        # self.
        pen=QPen()
        pen.setWidth(2)
        self.setPen(pen)
        self.nodes=[]
        
    def mousePressEvent(self,event):
        if event.button() == QtCore.Qt.LeftButton:
            print(event.pos())
            self.nodes.append(QGraphicsEllipseItem(-5,event.pos().y()-5,10,10,self))
        super().mousePressEvent(event)